cob.dashboards = cob.dashboards || {};
cob.dashboards.utils = cob.dashboards.utils || {};
cob.dashboards.queries = cob.dashboards.queries || {};

/**
 * Utility function that detects the presence of the attribute "component" in the divs and processes the corresponding
 * Dashboard component type using the mandatory div.id and the optional componentargs attribute.
 * The "component" value is the type of the component (ex: "totals" -> Totals Dashboard)
 * The "componentargs" is the full path of the query arguments; if not present is assumed to be in
                       cob.dashboards.queries[div.id]
 *
 * Ex: The <div id="todos" component="totals" componentargs="cob.dashboards.queries.special_todos" />
 *
 *
 * Receives an object with the dashboard constructors so that we don´t have to do the requires here
 * (and we would have to do them all)
 *
 * @param supportedDashboardComponents expects an object with the types and respective function constructores
 * Ex:
 * {
	"totals": TotalsDashboard
    }
 */
cob.dashboards.utils.parseDashboardComponents = function(supportedDashboardComponents) {
    var comps = document.querySelectorAll('[component]');
    var item;
    var type, argsObj, cArgs;

    for (var i = 0; i < comps.length; i++) {
        item = comps[i];

        if (item.id == '') window.console.error('The component div must have an ID');

        argsObj = item.getAttribute("componentargs");
        type = item.getAttribute("component");

        if (argsObj) {
            cArgs = cob.dashboards.utils._getObjectByName(argsObj);
        } else {//default namespace
            window.console.log("No componentargs attribute; will use the default namespace 'cob.dashboards.queries[" + item.id + "]'");
            cArgs = cob.dashboards.queries[item.id];
        }

        if (cob.dashboards.utils._isSupportedComponent(type)) {
            new supportedDashboardComponents[type](item.id, cArgs.version, cArgs.refreshMilis, cArgs.items, cArgs.defaultQueryHref, cArgs.defaultQueryURL);
        } else {
            window.console.error("CoB Dashboard component type not supported: '" + type + "'");
        }
    }

};

//TODO JBARATA: acrescentar aqui mais tipos de dashboards à medida q se forem implementando
cob.dashboards.utils._isSupportedComponent = function(type) {
    var supportedTypes = ["totals"];

    return supportedTypes.indexOf(type) >= 0;
};

cob.dashboards.utils._getObjectByName = function(objectFullName) {
    var namespaces = objectFullName.split(".");
    var obj = window;
    for (var i = 0; i < namespaces.length; i++) {
        obj = obj[namespaces[i]];
    }
    return obj;
};
